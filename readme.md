## Installation de Wamp server
http://www.wampserver.com/#wampserver-64-bits-php-5-6-25-php-7


##Ajout de php dans le Path
Aller dans les "Propriétés système"\
Puis "Variables d'environnement"\
Modifier la variable "Path" pour ajouter votre route vers php exemple : "C:\\wamp64\\bien\\php\\php5.6.25\

##Installation de composer
Télécharger le launcher sur : https://getcomposer.org/Composer-Setup.exe\

Vérifier que Composer soit bien installé, pour cela effectuer les quelques commandes suivantes dans votre invite de commande : \

- php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"\
- php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"\
- php composer-setup.php\
- php -r "unlink('composer-setup.php');"\

##Installation de notre projet
- Avec le fichier rar\
-- Se connecter à gitlab\
-- Télécharger le fichier compressé en rar\
-- L'extraire dans la destination voulu\
-- Copier le fichier .env-example et le renommé ".env"\
-- Dans votre invite de commande exécuter la commande suivante : "php artisan key:generate"\
-- Maintenant lançez votre serveur : "php arisan serve"\
-- Aller à l'adresse : "127.0.0.1:8000" dans votre navigateur web voilà !

- Télécharger avec le git bash\
-- Pour cela il faut installer git : https://git-scm.com/download/win\
-- Maintenant dans votre fichier de projet, effectuer un clic droit dans votre explorateur de fichier et cliquez sur "Git Bash Here"\
-- Tapez : "git init"\
-- Puis cette ligne tout en modifiant la partie "votre_nom_d'utilisateur"  :  "git add remote projetWeb https://votre_nom_d'utilisateur_git@gitlab.com/Dysey/projetWeb.git"\
-- "git pull projetWeb master"\
-- "composer update"\
-- Copier le fichier .env-example et le renommé ".env"\
-- Dans votre invite de commande exécuter la commande suivante : "php artisan key:generate"\
-- Maintenant lançez votre serveur : "php arisan serve"\
-- Aller à l'adresse : "127.0.0.1:8000" dans votre navigateur web voilà !

