<div class="evement">
    <div class="gauche col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="titre">
            <div class="titrehaut"></div>
            <h1>{!! $activite->titre !!}</h1>
            <p>{!! date('d-m-Y', strtotime($activite->date_evenement)) !!}</p>
            <p>{!! $activite->auteur !!}</p>
            <div class="titrebas"></div>
        </div>
        <p>{!! $activite->description !!}
            <br><br>
            <strong>PRIX : {!! $activite->prix !!} €</strong>
        </p>
    </div>

    <div class="droite col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <img src="{!! asset('img/activite/'.$activite->affiche)!!}" alt="Affiche evenement" class="affiche col-lg-9 col-md-10 col-sm-11 col-xs-12">
        <div class="affiche-btn col-lg-9 col-md-10 col-sm-11 col-xs-12">
            @if(Auth::check() && Auth::user()->droits != 3)
                <a href="{{ url('administration/event/'.$activite->ID_act.'/list') }}"><span class="glyphicon glyphicon-user"></span></a>
            @endif
            <a href="{{ url('/photo/'.$activite->ID_act)}}"><img src="{{asset ('img/picture.svg')}}" class="img-picture"></a>
            @if (Auth::guest())
            @else
            <a data-toggle="modal" id="solution" data-target="#myModal{{$activite->ID_act}}"><img src="{{asset ('img/plus.svg')}}" class="img-picture"></a>
            @endif
            @if(Auth::check() && Auth::user()->droits != 3)
                <a href="{{ url('administration/event/delete/'.$activite->ID_act) }}"><span class="glyphicon glyphicon-remove"></span></a>
            @endif
        </div>
        <!-- <div class="img-shadow"></div> -->
        <div class="bouton-jolie">
            <div></div>
            @if (Auth::check())
                <div class="bouton">
                    <img src="{{ asset('img/right-black.svg') }}" alt="flèche">
                    {{--<form action="{{ route('inscriptionActivite') }}" method="post"><button>Reservation</button></form>--}}
                    {{--<a href="{{ url('/reservation/'.$activite->ID_act) }}">RESERVATION</a>--}}
                    @if (\App\Http\Controllers\inscriptionController::exist($activite->ID_act) == null)
                        <a href="{{ url('/inscription/'.$activite->ID_act) }}}">Inscription</a>
                    @else
                        <a href="{{ url('/inscription/'.$activite->ID_act) }}">Désinscription</a>
                    @endif
                </div>
            @endif
            <div></div>
        </div>
    </div>
    <div class="border-vertical hidden-xs"></div>
</div>
<div class="line-separation hidden-xs"></div>

<!-- Modal -->
<div class="modal fade" id="myModal{{$activite->ID_act}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Ajouter photo</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route' => 'ajout.store','method'=>'POST','files'=>true)) !!}
                <div class="row">
                    <div>
                        <div class="form-group">
                            {!! Form::hidden('ID_act', $activite->ID_act, array('placeholder' => 'ID_act','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6">
                        <div class="form-group">
                            <strong>Mettre une photo:</strong>
                            {!! Form::file('url', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>