@extends('template')

@section('body')

    <div class="container">
        <p><a href="{{ url('/photo') }}">Photo</a> / {{ $photos->url }}</p>
            <h1>{{ $photos->ID_act }}</h1>

            <hr>

            <div class="row">
                <div class="col-md-4">
                    <img src="{{ asset('img/imageSociale/' . $photos->url) }}" alt="photos" class="img-responsive">
                </div>

        @foreach ($interested as $photo)
            <div class="col-md-3">
                <div class="thumbnail">
                    <div class="caption text-center">
                        <a href="{{ url('shop', [$photos->url]) }}"><img src="{{ asset('img/imageSociale/' . $photos->url) }}" alt="photos" class="img-responsive"></a>
                    </div> <!-- end caption -->

                </div> <!-- end thumbnail -->
            </div> <!-- end col-md-3 -->
        @endforeach

    </div> <!-- end row -->

    <div class="spacer"></div>


    </div> <!-- end container -->

@endsection
