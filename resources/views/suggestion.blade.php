@extends('template')

@section('titre', 'Suggestion')

@section('body')
    <section id="suggestion">
        <h2 class="text-center">UNE SUGGESTION A NOUS FAIRE ?</h2>
        <div>
            <form method="POST" action="{{ url('suggestion') }}">
                {{ csrf_field() }}
                <label>Message :</label>
                <textarea name="msg" maxlength="500" rows="4" class="col-lg-6 col-md-8 col-sm-10 col-xs-12"></textarea>
                <div class="bouton">
                    <img src="{{ asset('img/right-black.svg') }}" alt="flèche">
                    <button type="submit"><p>ENVOYER</p></button>
                </div>

            </form>
        </div>
    </section>
@endsection