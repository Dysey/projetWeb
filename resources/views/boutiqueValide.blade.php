@extends('template')

@section('body')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        Ce produit à été ajouté à la boutique
                    </div>
                    <div class="bouton-jolie">
                        <div></div>
                        <div class="bouton">
                            <img src="{{url('img/right-black.svg')}}" alt="flèche">
                            <a href="{{url('administration/boutique')}}"><input type="submit" name="" value="Retour à l'administration"></a>
                        </div>
                        <div></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection&