<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <!-- <link rel="icon" type="image/png" href="Images/CN.ico"/> -->
    <title>@yield('titre')</title>
    <link href="{{ asset('css/mycss2.0.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <script type="text/javascript" src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>


    <meta name="viewport" content="width=device-width">
    <meta name="description" content="Clément NEVEU - Etudiant informatique - Site web - 2016">
    <meta name="language" content="fr">
    <meta name="author" content="Clément NEVEU, Romain BELLIARD, Thomas LEMOINE">
    <meta name="keywords" content="clément, neveu, clement neveu, romain belliard, thomas lemoine, website, exia, exia.cesi, projet">
</head>

<body>
    <nav>
        <div id="flexbar" class="col-xs-12 hidden-xs">
            <div id="boutique" class="">
                <a href="{{ url('/shop') }}">Boutique</a>
            </div>
            <a href="{{ url('/') }}"><img src="{{ asset('img/logo.png') }}" alt="Logo BDE CESI" id="bde" class="hidden-xs hidden-sm"></a>
            <div id="navigation">
                <a href="{{ url('/') }}">Accueil</a>
                <a href="{{ url('/suggestion') }}">Suggestion</a>
                <div>
                    @if (Auth::guest())
                        <a href="{{ url('/home') }}">Connexion</a> | <a href="{{ url('/register') }}">Inscription</a>
                    @else
                        <a href="{{ url('/profile') }}">{{ Auth::user()->name }}</a> | <a href="{{ route('logout') }}"
                                                    onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Déconnexion</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    @endif
                </div>
            </div>
        </div>
        <label for="burger" class="visible-xs" id="label-burger">
            <div id="nav-icon2">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </label>
        <input type="checkbox" id="burger" class="hidden">
        <div id="nav-xs" class="visible-xs">
            <a href="{{ url('/') }}"><span class="glyphicon glyphicon-home">&nbsp;</span>Accueil</a>
            <a href="{{ url('/shop') }}"><span class="glyphicon glyphicon-shopping-cart">&nbsp;</span>Boutique</a>
            <a href="{{ url('/suggestion') }}"><span class="glyphicon glyphicon-bullhorn">&nbsp;</span>Suggestion</a>
            @if (Auth::guest())
                <a href="{{ url('/home') }}" class="red"><span class="glyphicon glyphicon-off">&nbsp;</span>Connexion</a> <a href="{{ url('/register') }}"><span class="glyphicon glyphicon-user">&nbsp;</span>Inscription</a>
            @else
                <a href="{{ url('/profile') }}"><span class="glyphicon glyphicon-user">&nbsp;</span>{{ Auth::user()->name }}</a> <a href="{{ route('logout') }}"
                                                                                  onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="red"><span class="glyphicon glyphicon-off">&nbsp;</span>Déconnexion</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            @endif
            <label for="burger" class="visible-xs">
                <div id="nav-icon2" class="open">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </label>
        </div>
        <div></div>
    </nav>
    @yield('body')
    <footer>
        <div class="line-hr">
        </div>
        <h2 class="text-center">© Tous droits réservés - 2017</h2>
        <h4><a href="{{url('/mentionlegal')}}">Mention légales</a> - <a href="mailto:clement.neveu1@viacesi.fr">Nous contacter</a></h4>
        <div id="social-network">
            <div>
                <a href="#"><img src="{{ asset('img/facebook.svg') }}" alt="Logo Facebbok"></a>
            </div>
            <div>
                <a href="#"><img src="{{ asset('img/twitter.svg') }}" alt="Logo Twitter"></a>
            </div>
            <div>
                <a href="#"><img src="{{ asset('img/google-plus.svg') }}" alt="Logo Google+"></a>
            </div>
        </div>
        <a href="https://exia.cesi.fr/"><img src="{{ asset('img/logoexia.jpg') }}" alt="logo exia"></a>
        {{--<h5>#EnjoyForFun</h5>--}}
    </footer>

</body>