@extends('template')

@section('titre', 'Accueil')

@section('body')
    @if(Auth::check() && Auth::user()->droits != 3)
        <div id="administration">
            <a href="{{ url('/administration') }}" class="hidden-xs">ADMINISTRATION</a>
            <a href="{{ url('/administration') }}" class="visible-xs"><span class="glyphicon glyphicon-cog"></span></a>
        </div>
    @endif
    <section id="presentation">
        <div class="img">
            <img src="{{ asset('img/equipe.jpg') }}" alt="Equipe BDE">
            <div class="blabla">
                <div class="containt-trait1">
                    <div class="trait1"></div>
                </div>
                <h4>BDE CESI EXIA</h4>
                <div class="hidden-xs">
                    <img src="{{ asset('img/right-arrow.svg') }}" alt="flèche" class="fleche">
                    <span>Lorem ipsum dolor sit amet, consectetuer adipiscing <br>
            elit, sed diam nonummy nibh euismod tincidunt ut <br>
            laoreet dolore magna aliquam erat volutpat. Ut wisi <br>
            enim ad minim veniam, quis nostrud exerci tation ullam</span>
                </div>
                <div>
                    <div class="trait2"></div>
                </div>
            </div>
        </div>
        <div class="containt-trait3">
            <div class="trait3"></div>
        </div>
        <h2 id="year" class="hidden-xs">2016/2017</h2>
    </section>

    <section id="backshape">
        <div id="actu">
        @foreach($activite as $activite)
            <!-- BLOCK EVENEMENT -->
            @include('evenement')
            <!-- END BLOCK EVENEMENT -->
        @endforeach
            <div class="bouton">
                <img src="{{ asset('img/right-black.svg') }}" alt="flèche">
                <a href="{{ url('/activite') }}">Voir tout les évenements</a>
            </div>

            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                {{--<ol class="carousel-indicators">--}}
                    {{--<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>--}}
                    {{--<li data-target="#carousel-example-generic" data-slide-to="1"></li>--}}
                    {{--<li data-target="#carousel-example-generic" data-slide-to="2"></li>--}}
                {{--</ol>--}}

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <!-- BLOCK SONDAGE -->
                        <div class="sondage">
                            <div class="titre">
                                <div class="titrehaut"></div>
                                <h1>Nous avons besoin de <span>votre avis</span> !</h1>
                                <div class="titrebas"></div>
                            </div>
                            <div class="choix">
                                <div class="choix1 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <h2>Laser Game</h2>
                                    <h5>30-04-2017</h5>
                                    <h5>Prix : 20 €</h5>
                                    <img src="{{ asset('img/bierre-cesi.jpg') }}" alt="Image événement 1">
                                    <div class="btn-vote">
                                        <div class="button-vote">
                                            <a href="#">VOTE</a>
                                        </div>
                                        <div class="btn-shadow"></div>
                                    </div>
                                </div>
                                <div class="choix2 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <h2>Bière et shop</h2>
                                    <h5>30-04-2017</h5>
                                    <h5>Prix : 10 €</h5>
                                    <img src="{{ asset('img/wave-cesi.png') }}" alt="Image événement 2">
                                    <div class="btn-vote">
                                        <div class="button-vote">
                                            <a href="#">VOTE</a>
                                        </div>
                                        <div class="btn-shadow"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END BLOCK SONDAGE -->
                    </div>
                    @foreach($sondages as $sondage)
                        <div class="item">
                        <!-- BLOCK SONDAGE -->

                            <div class="sondage">
                                <div class="titre">
                                    <div class="titrehaut"></div>
                                    <h1>Nous avons besoin de <span>votre avis</span> !</h1>
                                    <div class="titrebas"></div>
                                </div>

                                <div class="choix">
                                    <div class="choix1 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <h2>{{ $sondage->act1 }}</h2>
                                        <h5>{{ $sondage->date1 }}</h5>
                                        <h5>Prix : {{ $sondage->prix1 }} €</h5>
                                        <img src="{{ asset('img/bierre-cesi.jpg') }}" alt="Image événement 1">
                                        <div class="btn-vote">
                                            <div class="button-vote">
                                                <a href="#">VOTE</a>
                                            </div>
                                            <div class="btn-shadow"></div>
                                        </div>
                                    </div>
                                    <div class="choix2 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <h2>{{ $sondage->act2 }}</h2>
                                        <h5>{{ $sondage->date2 }}</h5>
                                        <h5>Prix : {{ $sondage->prix2 }} €</h5>
                                        <img src="{{ asset('img/wave-cesi.png') }}" alt="Image événement 2">
                                        <div class="btn-vote">
                                            <div class="button-vote">
                                                <a href="#">VOTE</a>
                                            </div>
                                            <div class="btn-shadow"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END BLOCK SONDAGE -->

                    </div>
                    @endforeach
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

        </div>
    </section>
    <script>
        $('#solution').addClass('hidden');
    </script>
@endsection