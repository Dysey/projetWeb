@extends('template')

@section('titre', 'Activité')

@section('body')
    @if(Auth::check() && Auth::user()->droits != 3)
        <div id="administration"><a href="{{ url('/administration') }}">ADMINISTRATION</a></div>
    @endif
        <div id="actu">
            @foreach($activite as $activite)
                <!-- BLOCK EVENEMENT -->
                @include('evenement')
                <!-- END BLOCK EVENEMENT -->
            @endforeach
        </div>
@endsection