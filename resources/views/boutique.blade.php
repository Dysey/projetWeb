@extends('template')

@section('titre', 'Administration de la boutique')

@section('body')
    <section id="admin">
        <ul class="nav nav-tabs nav-justified">
            <li role="presentation"><a href="{{ url('/administration/event') }}">Evenement</a></li>
            <li role="presentation"><a href="{{ url('/administration/sondage') }}">Sondage</a></li>
            <li role="presentation"  class="active"><a href="{{ url('/administration/boutique') }}">Boutique</a></li>
            <li role="presentation"><a href="{{ url('/administration/suggestion') }}">Suggestion</a></li>
        </ul>

        <div class="panel-group col-xs-12" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Ajout d'un produit
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        {!! Form::open(array('route' => 'boutique.store','method'=>'POST','files'=>true)) !!}
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group">
                                    <strong>Name:</strong>
                                    {!! Form::text('name', null, array('placeholder' => 'name','class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group">
                                    <strong>slug:</strong>
                                    {!! Form::text('slug', null, array('placeholder' => 'slug','class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group">
                                    <strong>price:</strong>
                                    {!! Form::text('price', null, array('placeholder' => 'price','class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="form-group">
                                    <strong>Upload File:</strong>
                                    {!! Form::file('image', array('class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>description:</strong>
                                    {!! Form::textarea('description', null, array('placeholder' => 'description','class' => 'form-control','style'=>'height:100px')) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection