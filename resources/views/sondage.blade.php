@extends('template')

@section('titre', 'Administration des sondages')

@section('body')
    <section id="admin">
        <ul class="nav nav-tabs nav-justified">
            <li role="presentation"><a href="{{ url('/administration/event') }}">Evenement</a></li>
            <li role="presentation" class="active"><a href="{{ url('/administration/sondage') }}">Sondage</a></li>
            <li role="presentation"><a href="{{ url('/administration/boutique') }}">Boutique</a></li>
            <li role="presentation"><a href="{{ url('/administration/suggestion') }}">Suggestion</a></li>
        </ul>

        <div class="panel-group col-xs-12" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Ajout d'un sondage
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <form action="{{url('administration/sondage')}}" method="POST" class="col-xs-12">
                            {{ csrf_field() }}
                            <h3>AJOUT</h3>
                            <label for="">Titre de l'activité 1</label>
                            <input type="text" name="act1" value="">
                            <br>
                            <label for="">Description 1</label>
                            <input type="text" name="description1" value="">
                            <br>
                            <label for="">Affiche 1</label>
                            <input type="file" name="" value="">
                            <br>
                            <label for="">Date de l'évenement 1</label>
                            <input type="date" name="date1" id="datepicker">
                            <br>
                            <label for="">Prix 1</label>
                            <input type="number" name="prix1" value="">
                            <br><br><br>
                            <label for="">Titre de l'activité 2</label>
                            <input type="text" name="act2" value="">
                            <br>
                            <label for="">Description 2</label>
                            <input type="text" name="description2" value="">
                            <br>
                            <label for="">Affiche 2</label>
                            <input type="file" name="" value="">
                            <br>
                            <label for="">Date de l'évenement 2</label>
                            <input type="date" name="date2" id="datepicker">
                            <br>
                            <label for="">Prix 2</label>
                            <input type="number" name="prix2" value="">


                            <div class="bouton-jolie">
                                <div></div>
                                <div class="bouton">
                                    <img src="{{url('img/right-black.svg')}}" alt="flèche">
                                    <a href="#"><input type="submit" value="ENVOYER"></a>
                                </div>
                                <div></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Administrer sondage
                        </a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                        <div>
                            <h3>Résultat sondage</h3>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <span class="badge">14</span>
                                    Evenement 1
                                </li>
                                <li class="list-group-item">
                                    <span class="badge">26</span>
                                    Evenement 2
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker();
        } );
    </script>
@endsection