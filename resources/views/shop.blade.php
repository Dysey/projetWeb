@extends('template')

@section('titre', 'Boutique')

@section('body')
    @if(Auth::check() && Auth::user()->droits != 3)
        <div id="administration"><a href="{{ url('/administration/boutique') }}" class="hidden-xs">ADMINISTRATION</a></div>
        <div id="administration"><a href="{{ url('/administration/boutique') }}" class="visible-xs"><span class="glyphicon glyphicon-cog"></span></a></div>
    @else
        <div id="administration"><a href="{{ url('/cart') }}" class="hidden-xs">PANNIER</a></div>
    @endif
    <section id="head-boutique">
        <div class="blabla2">
            <div class="containt-trait1">
                <div class="trait1"></div>
            </div>
            <h4>BOUTIQUE DU BDE</h4>
            <div class="hidden-xs">
                <img src="{{ asset('img/right-arrow.svg') }}" alt="flèche" class="fleche">
                <span>Lorem ipsum dolor sit amet, consectetuer adipiscing <br>
            elit, sed diam nonummy nibh euismod tincidunt ut <br>
            laoreet dolore magna aliquam erat volutpat. Ut wisi <br>
            enim ad minim veniam, quis nostrud exerci tation ullam</span>
            </div>
            <div>
                <div class="trait2"></div>
            </div>
        </div>
    </section>
    <section id="backshape2">
        <div id="list-produit">
            @foreach ($products->chunk(4) as $items)
                @foreach ($items as $product)
                    <div class="produit col-lg-3 col-md-4 col-sm-5 col-xs-12">
                        <div class="info-produit">
                            <a href="{{ url('shop', [$product->slug]) }}"><img src="{{ asset('img/' . $product->image) }}" alt="product" class="img-responsive"></a>
                            <a href="{{ url('shop', [$product->slug]) }}"><h3>{{ $product->name }}</h3>
                                <p>{{ $product->price }} €</p>
                            </a>
                        </div>
                        <div class="btn-groupe">
                            <div class="bouton">
                                <img src="{{ asset('img/right-black.svg') }}" alt="flèche">
                                <a href="{{ url('shop', [$product->slug]) }}">PLUS D'INFO</a>
                            </div>
                            @if(Auth::check() && Auth::user()->droits == 2)
                                <a href="{{ url('administration/boutique/delete/' . $product->id) }}"><span class="glyphicon glyphicon-remove"></span></a>
                            @endif
                        </div>
                    </div>
                @endforeach
            @endforeach
        </div>

    </section>
@endsection