@extends('template')

@section('titre', 'Administration des evenements')

@section('body')
    <section id="admin">

        <ul class="nav nav-tabs nav-justified">
            <li role="presentation" class="active"><a href="{{ url('/administration/event') }}">Evenement</a></li>
            <li role="presentation"><a href="{{ url('/administration/sondage') }}">Sondage</a></li>
            <li role="presentation"><a href="{{ url('/administration/boutique') }}">Boutique</a></li>
            <li role="presentation"><a href="{{ url('/administration/suggestion') }}">Suggestion</a></li>
        </ul>

        <div class="panel-group col-xs-12" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Ajout d'évenement
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <form action="{{ url('administration/event') }}" method="POST" class="col-xs-12" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <h3>AJOUT</h3>
                            <label for="">Titre de l'activité</label>
                            <input type="text" name="titre" value="">
                            <br>
                            <label for="">Description</label>
                            <input type="text" name="description" value="">
                            <br>
                            <label for="">Affiche</label>
                            <input type="file" name="affiche" value="">
                            <br>
                            <label for="">Date de l'évenement</label>
                            <input type="text" name="dateEvent" id="datepicker">
                            <br>
                            <label for="">Prix</label>
                            <input type="number" name="prix" value="">


                            <div class="bouton-jolie">
                                <div></div>
                                <div class="bouton">
                                    <img src="{{url('img/right-black.svg')}}" alt="flèche">
                                    <a href="#"><input type="submit" name="" value="ENVOYER"></a>
                                </div>
                                <div></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            Administrer evenement
                        </a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                        <form action="" method="post" class="col-xs-12">
                            <h3>MODIFICATION</h3>
                            <label for="">Titre de l'activité</label>
                            <select>
                                <option value="volvo">Activité 1</option>
                                <option value="saab">Activité 2</option>
                                <option value="mercedes">Activité 3</option>
                                <option value="audi">Activité 4</option>
                            </select>
                            <br>
                            <label for="">Description</label>
                            <input type="text" name="" value="">
                            <br>
                            <label for="">Affiche</label>
                            <input type="file" name="" value="">
                            <br>
                            <label for="">Date de l'évenement</label>
                            <input type="date" name="" value="">
                            <br>
                            <label for="">Prix</label>
                            <input type="number" name="" value="">


                            <div class="bouton-jolie">
                                <div></div>
                                <div class="bouton">
                                    <img src="{{url('img/right-black.svg')}}" alt="flèche">
                                    <a href="#"><input type="submit" name="" value="MODIFIER"></a>
                                </div>
                                <div></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker();
        } );
    </script>
@endsection