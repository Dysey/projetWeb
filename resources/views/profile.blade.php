@extends('template')

@section('titre', 'Mon Compte')

@section('body')
    <section id="mon-compte">
        <div id="hautrouge">
            <img src="{{ asset('img/logo.png') }}" alt="">
            <div class="HELLO">
                <div class="titrehaut"></div>
                <h1>Hello <span>{{Auth::user()->name}} {{Auth::user()->fullname}}</span> !</h1>
                <div class="titrebas"></div>
            </div>
        </div>
        <div id="info-moi" class="col-lg-8 col-sm-10 col-xs-12">
            <img src="{{ asset ('img/avatar/default_user.svg') }}" alt="Avatar">
            <form class="" action="{{url('profile')}}" method="post" enctype="multipart/form-data">
                <label for="avatar">Avatar</label>
                <input type="file" name="avatar" value="{{Auth::user()->avatar}}">
                <br>
                <label for="">Adresse mail</label>
                <input type="email" name="email" value="{{Auth::user()->email}}">
                <br>
                <label for="">Date de naissance</label>
                <input type="date" name="" value="{{Auth::user()->dateNaissance}}" disabled>
                <br>
                <label for="">Droit</label>
                <input type="text" name="" value="{{Auth::user()->droits}}" disabled>
                <br>
                <label for="">Ancien mot de passe</label>
                <input type="password" name="" value="">
                <br>
                <label for="">Nouveau mot de passse</label>
                <input type="password" name="new_pass" value="">
                <br>
                <label for="">Confirmez nouveau mot de passse</label>
                <input type="password" name="renew_pass" value="">
                <br>
                <label for="">Description/Contrainte</label>
                <textarea name="msg" maxlength="300" rows="4"></textarea>

                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

                <div class="bouton-jolie">
                    <div></div>
                    <div class="bouton">
                        <img src="{{ asset ('img/right-black.svg') }}" alt="flèche">
                        <a href="#"><input type="submit" name="" value="MODIFIER"></a>
                    </div>
                    <div></div>
                </div>
            </form>
        </div>



    </section>
@endsection