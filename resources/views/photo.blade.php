@extends('template')

@section('titre', 'Photo')

@section('body')
    <section id="photo">
        <h1>Titre de l'activité</h1>
        <h5>Date de l'évenement</h5>
        <div id="list-photo">
            @foreach ($photos->chunk(1) as $items)
                @foreach ($items as $photo)
                    <div class="produit col-lg-3 col-md-4 col-sm-5 col-xs-12">
                        <div class="info-photo">
                            <a href="{{ url('img/imageSociale', [$photo->url]) }}"><img src="{{ asset('img/imageSociale/' . $photo->url) }}" alt="product" class="img-responsive"></a>
                            <div class="boutonlike">
                                    @if (\App\Http\Controllers\photoController::exist($photo->ID_photo) == null)
                                        <a href="{{ url('photo/like', [$photo->ID_photo]) }}"><img src="{{ asset ('img/like.svg') }}" alt="like"></a>
                                    @else
                                        <a href="{{ url('photo/like', [$photo->ID_photo]) }}"><img src="{{ asset ('img/dislike.svg') }}" alt="dislike"></a>
                                    @endif
                            </div>

                            @if(Auth::check() && Auth::user()->droits == 2)
                                <div class="boutonlike">
                                    <a href="{{ url('photo/like/'.$photo->ID_photo.'/delete') }}"><span class="glyphicon glyphicon-remove"></span></a>
                                </div>
                            @endif

                        </div>
                    </div>
                @endforeach
            @endforeach
    </section>
@endsection