<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActiviteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Activite', function (Blueprint $table){
           $table->increments('ID_act');
           $table->string('titre', 255);
           $table->string('description', 1000);
           $table->string('affiche', 255);
           $table->date('date_publication');
           $table->date('date_evenement');
           $table->integer('prix');
           $table->string('auteur', 100);
           $table->string('recurrence', 100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Activite');
    }
}
