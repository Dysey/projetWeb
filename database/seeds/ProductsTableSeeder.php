<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'Mug BDE',
            'slug' => 'Mug-bde',
            'description' => 'description goes here',
            'price' => 9,
            'image' => 'mug.jpg',
        ]);

        DB::table('products')->insert([
            'name' => 'Tee-Shirt mixte noir',
            'slug' => 'Tee-Shirt-noir',
            'description' => 'description goes here',
            'price' => 19.99,
            'image' => 'Tee-shirt_noir.jpg',
        ]);

        DB::table('products')->insert([
            'name' => 'Tee-Shirt mixte rouge',
            'slug' => 'Tee-Shirt-rouge',
            'description' => 'description goes here',
            'price' => 19.99,
            'image' => 'Tee-shirt_rouge.jpg',
        ]);

        DB::table('products')->insert([
            'name' => 'Tee-Shirt mixte bleu',
            'slug' => 'Tee-Shirt-bleu',
            'description' => 'description goes here',
            'price' => 19.99,
            'image' => 'Tee-shirt-bleu.jpg',
        ]);

        DB::table('products')->insert([
            'name' => 'Verre BDE',
            'slug' => 'Verre-BDE',
            'description' => 'description goes here',
            'price' => 5.99,
            'image' => 'gobelet.png',
        ]);

        DB::table('products')->insert([
            'name' => 'sac BDE',
            'slug' => 'sac_BDE',
            'description' => 'description goes here',
            'price' => 29.99,
            'image' => 'sac.jpg',
        ]);
        DB::table('products')->insert([
            'name' => 'Clé USB 8Go',
            'slug' => 'cle_USB_8Go',
            'description' => 'description goes here',
            'price' => 9.99,
            'image' => 'cle_usb.jpg',
        ]);

    }
}
