<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Like extends Model
{
    use SoftDeletes;

    protected $table = 'likes';

    protected $fillable = [
        'user_id',
        'id_photo',
        'likeable_type',
    ];

    /**
     * Get all of the products that are assigned this like.
     */
    public function products()
    {
        return $this->morphedByMany('App\Product', 'like');
    }

    /**
     * Get all of the posts that are assigned this like.
     */
    public function posts()
    {
        return $this->morphedByMany('App\Post', 'like');
    }
}