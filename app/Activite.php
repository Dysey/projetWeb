<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activite extends Model
{
    protected $table = 'activite';
    public $timestamps = false;
    protected $primaryKey = 'ID_act';
    
    protected $fillable = [
      'ID_act', 'titre', 'description', 'affiche', 'prix',
    ];

    protected  $hidden = [

    ];


    public function users(){
        return $this->belongsToMany(user::class, 'Inscrit_Act', 'ID_act', 'ID_user');
    }

}