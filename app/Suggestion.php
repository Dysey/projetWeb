<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suggestion extends Model
{
    protected $table = 'Suggestion';
    public $timestamps = false;
    protected $primaryKey = 'ID_suggestion';

    protected $fillable = [
        'msg',
    ];

    protected  $hidden = [

    ];
}
