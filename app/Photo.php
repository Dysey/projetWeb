<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class Photo extends Model
{
    protected $table = 'Photo';

    public $fillable = ['ID_act'];
    public $timestamps=false;
    protected $primaryKey = 'ID_photo';

    public function likes()
    {
        return $this->morphToMany('App\User', 'like')->whereDeletedAt(null);
    }

    public function getIsLikedAttribute($id)
    {
        $like = $this->likes()->whereid(Auth::id());
        return (!is_null($like)) ? true : false;
    }
}