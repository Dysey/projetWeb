<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'fullname', 'dateNaissance', 'email', 'avatar', 'password', 'droits'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function activites(){
        return $this->belongsToMany(activite::class, 'Inscrit_Act', 'ID_user', 'ID_act');
    }

    public function likedPosts()
    {
        return $this->morphedByMany('App\Post', 'like')->whereDeletedAt(null);
    }
}
