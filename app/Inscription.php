<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inscription extends Model
{
    protected $table = 'Inscrit_Act';
    public $timestamps = false;
}
