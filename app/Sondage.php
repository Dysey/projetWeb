<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sondage extends Model
{
    protected $table = 'Post_Activite';
    public $timestamps = false;
/*    const CREATED_AT = 'date_publication';*/
    protected $primaryKey = 'ID_sondage';

    protected $fillable = [
        'ID_sondage', 'Act1', 'Act2', 'result1', 'result2', 'date1', 'date2', 'date_publication', 'description1', 'description2'
   ];

    protected  $hidden = [

    ];
}
