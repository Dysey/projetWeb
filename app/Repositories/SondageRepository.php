<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 18/04/2017
 * Time: 15:58
 */

namespace App\Repositories;

Use App\Sondage;

class SondageRepository
{
    protected $sondage;

    public function __construct(Sondage $sondage){
        $this->sondage = $sondage;
    }

    public function getPaginate(){
        return $this->sondage->orderBy('date_publication', 'asc')->get();
    }

    public function destroy($id){
        $this->sondage->findOrFail($id)->delete();
    }
}