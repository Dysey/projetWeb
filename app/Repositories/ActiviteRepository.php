<?php


namespace App\Repositories;

Use App\Activite;
use App\Http\Controllers\inscriptionController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class ActiviteRepository{

    protected $activite;
    protected $inscription;

    public function __construct(Activite $activite, inscriptionController $inscription){
        $this->activite = $activite;
        $this->inscription = $inscription;
    }

    private function save(Activite $activite, Array $inputs, $filename){
        $activite->titre = $inputs['titre'];
        $activite->description = $inputs['description'];
        $affi = "";
        if(isset($inputs['affiche'])){
            $affi = $filename;
        } else {
            $affi = "default_affiche.png";
        }
        $activite->affiche = $affi;


        $activite->date_publication = date("Y-m-d H:i:s");
        $activite->date_evenement = $inputs['dateEvent'];
        $activite->prix = $inputs['prix'];
        $activite->auteur = Auth::user()->name;
        $activite->recurrence = isset($inputs['auteur']);

        $activite->save();
    }

    public function getPaginate(){
        return $this->activite->orderBy('date_evenement', 'asc')->get();
    }

    public function store(Array $inputs, $filename){
        $activite = $this->activite;
        $this->save($activite, $inputs, $filename);

        return $activite;
    }

    public function getById($id){
        return $this->activite->findOrFail($id);
    }

    public function getLast(){
        return $this->activite->orderBy('date_evenement', 'asc')->paginate(1);
    }

    public function update($id, Array $inputs){
        $this->save($this->getById($id), $inputs);
    }

    public function destroy($id){
        $this->inscription->destroy($id);
        $this->getById($id)->delete();
    }

}