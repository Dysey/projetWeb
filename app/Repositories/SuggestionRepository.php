<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 18/04/2017
 * Time: 15:58
 */

namespace App\Repositories;

Use App\Suggestion;

class SuggestionRepository
{
    protected $suggestion;

    public function __construct(Suggestion $suggestion){
        $this->suggestion = $suggestion;
    }

    public function getPaginate(){
        return $this->suggestion->orderBy('date_suggestion', 'asc')->get();
    }

    public function destroy($id){
        $this->suggestion->findOrFail($id)->delete();
    }
}