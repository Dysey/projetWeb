<?php
/**
 * Created by PhpStorm.
 * User: Wickasana
 * Date: 19/04/2017
 * Time: 10:38
 */

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Photo;

class ajoutPhotoController extends controller
{
    public function index(){
        return view('activite');
    }
    public function create(){

    }
    public function store(Request $request) {

        $this->validate($request, [
            'ID_act' => 'required',
            'url' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $url = new Photo($request->input()) ;

        if($file = $request->hasFile('url')) {

            $file = $request->file('url') ;

            $fileName = $file->getClientOriginalName() ;
            $destinationPath = public_path().'/img/imageSociale/' ;
            $file->move($destinationPath,$fileName);
            $url->url = $fileName ;
        }
        $url->save() ;
        return redirect('photo/'.$request->ID_act);
    }
}