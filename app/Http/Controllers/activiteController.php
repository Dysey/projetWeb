<?php

namespace App\Http\Controllers;
use App\Http\Requests\ActiviteCreateRequest;
use App\Http\Requests\ActiviteUpdateRequest;
use Illuminate\Support\Facades\Auth;

use App\Repositories\ActiviteRepository;


class ActiviteController extends Controller
{
    protected $activiteRepository;
    protected $nbrPerPage = 5;

    public function __construct(ActiviteRepository $activiteRepository)
    {
        $this->activiteRepository = $activiteRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $activite = $this->activiteRepository->getPaginate();
        return view('activite', compact('activite'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::check() && Auth::user()->droits != 3){
            return view('event');
        } else {
            return redirect('accueil');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ActiviteCreateRequest $request)
    {
        if($file = $request->hasFile('affiche')) {

            $file = $request->file('affiche') ;

            $fileName = $file->getClientOriginalName() ;
            $destinationPath = public_path().'/img/activite/' ;
            $file->move($destinationPath,$fileName);
        }
        $activite = $this->activiteRepository->store($request->all(), $fileName);


        return redirect('activite')->withOk("L'activité " . $activite->name . " a été créée.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $activite = $this->activiteRepository->getById($id);
        return view('show', compact('activite'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activite = $this->activiteRepository->getById($id);
        return view('edit', compact('activite'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ActiviteUpdateRequest $request, $id)
    {
        $this->activiteRepository->update($id, $request->all());

        return redirect('activite')->withOk("L'activité " . $request->input('name') . " a été modifiée.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->activiteRepository->destroy($id);

        return back();
    }
}