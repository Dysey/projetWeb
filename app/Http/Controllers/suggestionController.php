<?php

namespace App\Http\Controllers;

use App\Suggestion;
use Illuminate\Http\Request;
use App\Http\Requests\SuggestionCreateRequest;
use Illuminate\Support\Facades\Auth;

use App\Repositories\SuggestionRepository;

class suggestionController extends Controller
{
    protected $suggestionRepository;

    public function __construct(SuggestionRepository $suggestionRepository)
    {
        $this->suggestionRepository = $suggestionRepository;
    }

    public function showAll(){
        $suggestion = $this->suggestionRepository->getPaginate();
        return view('listSuggestion', compact('suggestion'));
    }

    public function getForm(){
        if(Auth::check()){
            return view('suggestion');
        } else {
            return redirect('accueil');
        }
    }

    public function postForm(SuggestionCreateRequest $request){

        //$suggestion = $request->msg;
            $suggestion = new Suggestion;
            $suggestion->msg = $request->msg;
            $suggestion->ID_user = Auth::user()->id;
            $suggestion->date_suggestion = date("Y-m-d H:i:s");
        //$suggestion = $this->suggestionRepository->store($request->all());

        //return view('suggestionValide')->withOk("Votre suggestion a été créée.");
        $suggestion->save();
        return view('suggestionValide');
    }

    public function destroy($id){
        $this->suggestionRepository->destroy($id);

        return back();
    }

}
