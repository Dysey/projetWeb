<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\product;
use Illuminate\Support\Facades\Auth;

class administrationBoutiqueController extends Controller
{
    public function index(){
        if(Auth::check() && Auth::user()->droits == 2){
            return view('boutique');
        } else {
            return redirect('accueil');
        }
    }
    public function create(){

    }
    public function store(Request $request) {

        $this->validate($request, [
            'name' => 'required',
            'price' => 'required',
            'slug' => 'required',
            'description'=>'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $product = new Product($request->input()) ;

        if($file = $request->hasFile('image')) {

            $file = $request->file('image') ;

            $fileName = $file->getClientOriginalName() ;
            $destinationPath = public_path().'/img/' ;
            $file->move($destinationPath,$fileName);
            $product->image = $fileName ;
        }
        $product->save() ;
        return view('boutiqueValide');
    }
}
