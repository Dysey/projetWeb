<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use Illuminate\Contracts\Auth;
class profileController extends Controller
{
    public function index(){

            return view('profile');

    }

    public function store() {

        Profile::table('users')
            ->where('avatar', Auth::user()->avatar)  // find your user by their email
            ->limit(1)  // optional - to ensure only one record is updated.
            ->update(array('avatar' => $avatar));  // update the record in the DB.

        return user_name;
    }
}
