<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Photo;
use App\Like;
use Illuminate\Support\Facades\Auth;

class photoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $photo;
    protected $like;
    protected $nbrPerPage = 5;

    public function __construct(Photo $photo, Like $like)
    {
        $this->photo = $photo;
        $this->like = $like;
    }

    public function index($ID_act)
    {
        $photos = Photo::all();
        return view('photo', compact('photos'));
    }


    /**
     * Display the specified resource.
     *
     * @param  string  $url
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $photos = $this->photo->where('ID_act', $id)->get();
        //$interested = Photo::where('ID_act', '!=', $ID_act)->get();

        return view('photo', compact('photos'));
    }


    public static function exist($id){
        $exist = Like::where('user_id', Auth::id())->where('ID_photo', $id)->first();
        return $exist;
    }

    public function deletePhoto($id)
    {
        $this->photo->findOrFail($id)->delete();
        return redirect()->back();
    }
}
