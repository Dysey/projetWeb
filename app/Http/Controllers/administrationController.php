<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class administrationController extends Controller
{
    public function index() {
        if(Auth::check() && Auth::user()->droits != 3){
            return view('event');
        } else {
            return redirect('accueil');
        }
    }

    public function event() {
        if(Auth::check() && Auth::user()->droits != 3){
            return view('event');
        } else {
            return redirect('accueil');
        }
    }

    public function sondage() {
        if(Auth::check() && Auth::user()->droits != 3){
            return view('sondage');
        } else {
            return redirect('accueil');
        }
    }

    public function boutique() {
        if(Auth::check() && Auth::user()->droits != 3){
            return view('boutique');
        } else {
            return redirect('accueil');
        }
    }

}
