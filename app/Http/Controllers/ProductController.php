<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Product;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{

    protected $product;
    public function __construct(Product $product){
        $this->product = $product;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        if(Auth::check()){
            return view('shop', compact('products'));
        } else {
            return redirect('accueil');
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $product = Product::where('slug', $slug)->firstOrFail();
        $interested = Product::where('slug', '!=', $slug)->get()->random(4);

        return view('product', compact('product') ,compact('interested'));
    }

    public function destroy($id)
    {
        if(Auth::check() && Auth::user()->droits == 2) {
            $this->product->findOrFail($id)->delete();
            return back();
        } else {
            return redirect('shop');
        }
    }


}
