<?php

namespace App\Http\Controllers;

use App\Like;
use Illuminate\Support\Facades\Auth;

class LikeController extends Controller
{

    public function __construct(Like $like){
        $this->like = $like;
    }

    public function likePhoto($id)
    {
        // here you can check if product exists or is valid or whatever

        $this->handleLike('App\Photo', $id);
        return redirect()->back();
    }


    public function likePost($id)
    {
        // here you can check if product exists or is valid or whatever

        $this->handleLike('App\Post', $id);
        return redirect()->back();
    }

    public function handleLike($type, $id)
    {
        $existing_like = Like::withTrashed()->whereLikeableType($type)->whereid_photo($id)->whereUserId(Auth::id())->first();

        if (is_null($existing_like)) {
            Like::create([
                'user_id'       => Auth::id(),
                'id_photo'   => $id,
                'likeable_type' => $type,
            ]);
        } else {
            //Like::where('ID_photo', $id)->where('user_id', Auth::id())->delete();
            if (is_null($existing_like->deleted_at)) {
                $existing_like->forceDelete();
            }
        }
    }
//
}