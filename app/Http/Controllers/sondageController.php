<?php

namespace App\Http\Controllers;

use App\Http\Requests\SondageCreateRequest;
use App\Sondage;
use Illuminate\Http\Request;
use App\Repositories\SondageRepository;
use Illuminate\Support\Facades\Auth;

class sondageController extends Controller
{
    protected $sondageRepository;

    public function __construct(SondageRepository $sondageRepository)
    {
        $this->sondageRepository = $sondageRepository;
    }

    public function index() {
        if(Auth::check() && Auth::user()->droits != 3){
            return view('sondage');
        } else {
            return redirect('accueil');
        }
    }

    public function postForm(SondageCreateRequest $request){

        $sondage = new Sondage;
        $sondage->act1 = $request->act1;
        $sondage->description1 = $request->description1;
        $sondage->prix1 = $request->prix1;
        $sondage->date1 = $request->date2;

        $sondage->act2 = $request->act2;
        $sondage->description2 = $request->description2;
        $sondage->prix2 = $request->prix2;
        $sondage->date2 = $request->date2;

        $sondage->date_publication = date("Y-m-d H:i:s");

        $sondage->save();
        return view('suggestionValide');
    }

    public function destroy($id){
        $this->sondageRepository->destroy($id);

        return back();
    }
}
