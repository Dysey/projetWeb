<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class userController extends Controller
{
    protected $user;
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index()
    {
        $users = user::with('activite')->get();

        return view('users.index')->with([
            'users' => $users
        ]);
  }
  public static function add_name($id){
      $user_name = User::where('id', $id)->addselect('name')->addselect('fullname')->first('name');
      return $user_name;
  }

}
