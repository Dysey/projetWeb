<?php

namespace App\Http\Controllers;
use App\User;
use App\Activite;
use App\Inscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class inscriptionController extends Controller
{
    protected $activite;
    protected $inscription;
    protected $user;

    public function __construct(Activite $activite, User $user, Inscription $inscription)
    {
        $this->activite = $activite;
        $this->user = $user;
        $this->inscription = $inscription;
    }

    public function store($id){

        //$activite->users()->attach(Auth::user());
        //return view('inscription')->withOk("Votre inscription à bien été pris en compte");
        //$user = App\User::find(1);
        //dd($this->user);
        //dd(Auth::user()->activites);
        //Auth::user()->activites()->attach(Auth::user());
        //return view('inscription.validation')->withOk("Votre inscription à bien était pris en compte");
        //$activite = App\Activite::find(1);
        //$this->activite->user()->attach();
        $this->handleLike($id);
        return redirect()->back();
    }

    public function handleLike($id)
    {
        $existing_like = $this->inscription->where('ID_act', $id)->where('ID_user', Auth::id())->first();

        if (is_null($existing_like)) {
            $activite = Activite::findOrFail($id);
            Auth::user()->activites()->attach($activite);
        } else {
            //Like::where('ID_photo', $id)->where('user_id', Auth::id())->delete();

            $existing_like->forceDelete();

        }
    }

    public function getByEvent($id){
        if(Auth::check() && Auth::user()->droits == 1) {
            $liste = $this->inscription->where('ID_act', $id)->addSelect('ID_user')->get();
            return view('listInscription', compact('liste'));
        } else {
            return redirect('accueil');
        }
    }

    public function destroy($id){
        $activite = Activite::findOrFail($id);
        Auth::user()->activites()->detach($activite);
    }

    public static function exist($id){
        $exist = Inscription::where('ID_user', Auth::id())->where('ID_act', $id)->first();
        return $exist;
    }

}
