<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ActiviteRepository;
use App\Repositories\SondageRepository;

class accueilController extends Controller
{
    public function __construct(ActiviteRepository $activiteRepository, SondageRepository $sondageRepository)
    {
        $this->activiteRepository = $activiteRepository;
        $this->sondageRepository = $sondageRepository;
    }

    public function index() {
        $activite = $this->activiteRepository->getLast();
        $sondages = $this->sondageRepository->getPaginate();
        return view('accueil', compact('activite'), compact('sondages'));
    }
}
