<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SondageCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'act1' => 'required|max:255',
            'description1' => 'required|max:1000',
            'date1' => 'required|date',
            'prix1' => 'required|max:11',
            'act2' => 'required|max:255',
            'description2' => 'required|max:1000',
            'date2' => 'required|date',
            'prix2' => 'required|max:11'
        ];
    }
}
