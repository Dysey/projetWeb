<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'accueilController@index');
Route::get('/accueil', 'accueilController@index');

Route::auth();
Route::get('/home', 'HomeController@index');

Route::resource('activite', 'activiteController');
Route::get('inscription/{id}', 'inscriptionController@store');

Route::get('/administration', 'administrationController@index');


Route::get('/administration/event', 'activiteController@create');
Route::post('/administration/event', 'activiteController@store');
Route::get('/administration/event/{id}/list', 'inscriptionController@getByEvent');
Route::get('/administration/event/delete/{id}', 'activiteController@destroy');


Route::get('/administration/sondage', 'sondageController@index');
Route::post('/administration/sondage', 'sondageController@postForm');

Route::resource('/administration/boutique', 'administrationBoutiqueController');
//Route::post('/administration/store', 'administrationBoutiqueController@store');

Route::get('/administration/suggestion', 'suggestionController@showAll');
Route::get('/administration/suggestion/delete/{id}', 'suggestionController@destroy');

Route::get('suggestion', 'suggestionController@getForm');
Route::post('suggestion', 'suggestionController@postForm');

Route::resource('/profile', 'profileController');

Route::get('photo/{id}', 'photoController@show');

Route::resource('shop', 'ProductController', ['only' => ['index', 'show']]);
Route::get('administration/boutique/delete/{id}', 'ProductController@destroy');

Route::resource('cart', 'CartController');
Route::delete('emptyCart', 'CartController@emptyCart');

Route::resource('activite/ajout','ajoutPhotoController');

Route::get('/mentionlegal', 'mentionlegalController@index');

Route::get('post/like/{id}', ['as' => 'post.like', 'uses' => 'LikeController@likePost']);
Route::get('photo/like/{id}', ['as' => 'photo.like', 'uses' => 'LikeController@likePhoto']);
Route::get('photo/like/{id}/delete', 'photoController@deletePhoto');